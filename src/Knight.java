public class Knight extends Fighter {
    public Knight(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        if (Utility.isSquare(Battle.GROUND)) {
            return this.getBaseHp() * 2;
        }
        else if (this.getWp() == 1) {
            return this.getBaseHp();
        }
        return (double) this.getBaseHp() / 10;
    }
}
