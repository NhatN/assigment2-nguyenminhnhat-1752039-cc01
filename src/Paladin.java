public class Paladin extends Knight {
    public Paladin(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        int x = 1;
        int y = 1;
        int z = x + y;
        int fibIndex = 3;
        while (z <= this.getBaseHp()) {
            if (z == this.getBaseHp()) {
                return 1000 + fibIndex;
            }
            x = y;
            y = z;
            z = x + y;
            fibIndex++;
        }
        return this.getBaseHp() * 3;
    }
}
